<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{$title}</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    {*<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>*}

    <script type="text/javascript" src="/style/js/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
    <script type="text/javascript" src="/style/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="/style/js/tools.js"></script>
    <script  type="text/javascript" type="text/javascript" src="/style/js/app.js"></script>
    <link rel="stylesheet" href="/style/css/style.css">

</head>
<body>
<header class="header" >
    <div class="container">
        <div class="row">
            <div class="col-md-3 logo">

                <img src="/style/img/logo.png" width="70px" height="70px">
            </div>
            <div class="col-md-6 logo">
            </div>
            <div class="col-md-3 logo phone">
                <span>Phone:</span> +38 097 007 4828 <br>
                <span>Email:</span> olga.che26.09@gmail.com
            </div>
        </div>
    </div>
</header>
<nav class="menu bs-docs-nav navbar navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle ololo" data-target="#bs-navbar" data-toggle="collapse" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand">Project</a>
        </div>
        <nav class="collapse navbar-collapse" id="bs-navbar">
            <ul class="nav navbar-nav navbar-right">
                {foreach from=$menu key=k item=v}
                    <li class="active">
                        <a href="{$v}">{$k}</a>
                    </li>
                {/foreach}
            </ul>

        </nav>
    </div>
</nav>