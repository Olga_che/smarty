<section class="content">
    <div class="container content-login">
        <div class="col-md-4"></div>

        <div class="col-md-4">
            <form class="form-signin" method="post" id="loginForm">
                <div class="login_div">
                    <h2 class="form-signin-heading">Login</h2>
                </div>
                <div class="login_div">
                    <label for="inputEmail" class="sr-only">Email</label>
                    <input name="inputEmail" type="email" id="inputEmail" class="form-control" placeholder="Email">
                </div>
                <div class="login_div">
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input name="inputPassword" type="password" id="inputPassword" class="form-control" placeholder="Password">
                </div>
                <div class="login_div">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                </div>
            </form>
        </div>

        <div class="col-md-4"></div>

    </div>
    </div>


</section>