<section class="content">
    <div class="container content-login">
        <div class="col-md-4"></div>

        <div class="col-md-4">
            <form class="form-registration" method="post" id="registerForm">
                <div class="login_div">
                    <h2 class="form-signin-heading">{$title}</h2>
                </div>
                <div class="login_div">
                    <label for="inputName" class="sr-only">Name</label>
                    <input name="inputName" type="text" id="inputName" class="form-control" placeholder="Name">
                </div>
                <div class="login_div">
                    <label for="inputEmail" class="sr-only">Email</label>
                    <input name="inputEmail" type="email" id="inputEmail" class="form-control" placeholder="Email">
                </div>
                <div class="login_div">
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input name="inputPassword" type="password" id="inputPassword" class="form-control" placeholder="Password">
                </div>
                <div class="login_div">
                    <label for="inputPasswordRepeat" class="sr-only">Repeat password</label>
                    <input name="inputPasswordRepeat" type="password" id="inputPasswordRepeat" class="form-control" placeholder="Repeat Password">
                </div>
                <div class="login_div">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Registration</button>
                </div>
            </form>
        </div>

        <div class="col-md-4"></div>

    </div>
    </div>


</section>
