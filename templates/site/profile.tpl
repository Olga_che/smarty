
<script>
</script>

    <section class="content">
    <div class="container content-page">
        <div class="row">
            <div class="col-xs-12 col-md-3 col-md-push-9">
                {$dataimg}
            </div>
            <div class="col-xs-12 col-md-9 col-md-pull-3 page_main">
                <div class="modal fade" role="dialog" id="my_personal_inform" tabindex="-1"
                     style="display: none;">
                    <div class="modal-dialog" role="document"> <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="my_personal_inform_дфиуд">Сhange written</h4>
                            </div>
                            <div class="modal-body">
                                <form action="javascript:void(0);" name="formProfile" class="form-data" id="formProfileformProfile">
                                    <div class="login_div">
                                        <label for="inputName" class="sr-only">Name</label>
                                        <input name="inputName" value="{$data.name}" type="text" id="inputName" class="form-control" placeholder="Name">
                                    </div>
                                    <div class="login_div">
                                        <label for="inputPhone" class="sr-only">Phone</label>
                                        <input name="inputPhone"  value="{$data.phone}" type="text" id="inputPhone" class="form-control" placeholder="Phone">
                                    </div>
                                    <div class="login_div">
                                        <label for="inputEmail" class="sr-only">Email</label>
                                        <input  name="inputEmail"  value="{$data.email}"type="email" id="inputEmail" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="login_div">
                                        <label for="textareaEducation" class="sr-only">Education</label>
                                        <textarea  name="textareaEducation" id="textareaEducation"  class="form-control"  placeholder="Education">{$data.education}</textarea>
                                    </div>
                                    <div class="login_div">
                                        <label for="textareaSkills" class="sr-only">Professional skills</label>
                                        <textarea  name="textareaSkills" id="textareaSkills"  class="form-control"  placeholder="Professional skills">{$data.skills}</textarea>

                                    </div>
                                    <div class="login_div">
                                        <label for="inputInformation" class="sr-only">Additional information</label>
                                        <textarea  name="inputInformation" id="textareaSkills"  class="form-control"  placeholder="Additional information">{$data.information}</textarea>

                                    </div>

                                    <div class="login_div">
                                        <div class="divError" id="error"></div>
                                        <input name="updateData" type="submit" value="Save changes" id="updateData" class="btn btn-primary" onclick="sendform();">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <p class="pull-right profile_button">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#my_personal_inform">Change profile</button>
                </p>

                <h2 class="js_name">{$data.name} </h2>


                <p></p>
                <hr>
                <p><b>Phone: </b> <span class="js_phone">{$data.phone}</span> </p>
                <p><b>Email: </b> <span class="js_email">{$data.email}</span></p>

                <p></p>
                <hr>
                <div class="page_head">Education</div>
                <p class="js_education">{$data.education}</p>

                <div class="page_head">Professional skills</div>
                <p class="js_skills">{$data.skills}</p>
                <div class="page_head">Additional information</div>
                <p class="js_information">{$data.information}</p>



            </div>

        </div>


    </div>


</section>