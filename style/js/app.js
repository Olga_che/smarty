var $ = window.jQuery;
function sendform() {
    var msg = $('#formProfile').serialize();
    $.ajax({
        type: 'POST',
        url: "/ajax.php",
        data: msg + "&action=sendform",
        cache: false,
        dataType: "json"
    }).done(
        function (data) {
            $(".js_name").html(data.name);
            $(".js_phone").html(data.phone);
            $(".js_email").html(data.email);
            $(".js_education").html(data.education);
            $(".js_skills").html(data.skills);
            $(".js_information").html(data.information);

        }
    );

}