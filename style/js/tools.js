$(document).ready(function(){
    $("#loginForm").validate({
        rules:{
            inputEmail:{
                email: true,
                required: true
            },
            inputPassword:{
                required: true,
                minlength: 3,
                maxlength: 16
            }
        },
        messages:{
            inputEmail:{
                required: "Required field"
            },
            inputPassword:{
                required: "Required field",
                minlength: "Min 3 symbol",
                maxlength: "Max symbol - 16"
            }

        }

    });
    $('#registerForm').validate({
        rules:{
            inputName:{
                required: true,
                minlength: 3,
                maxlength: 16
            },
            inputEmail:{
                required: true,
                email: true,
                minlength: 3
            },
            inputPassword:{
                required: true,
                minlength: 3,
                maxlength: 16
            }
        },
        messages:{
            inputName:{
                required: "Required field",
                minlength: "Min 3 symbol",
                maxlength: "Max symbol - 16"
            },
            inputEmail:{
                required: "Required field",
                email: true,
                minlength: "Min 3 symbol"
            },
            inputPassword:{
                required: "Required field",
                minlength: "Min 3 symbol",
                maxlength: "Max symbol - 16"
            }

        }

    });
    $("#formProfile").validate({
        rules:{
            inputName:{
                required: true,
                minlength: 3,
                maxlength: 16
            },
            inputPhone:{
                required: true,
                minlength: 10,
                maxlength: 13
            },
            inputEmail:{
                required: true,
                email: true,
                minlength: 3
            },
            inputPassword:{
                required: true,
                minlength: 3,
                maxlength: 16
            }
        },
        messages:{

            inputName:{
                required: "Required field",
                minlength: "Min 3 symbol",
                maxlength: "Max symbol - 16"
            },

            inputPhone:{
                required: "Required field",
                minlength: "Min 10 symbol",
                maxlength: "Max symbol - 13"
            },
            inputEmail:{
                required: "Required field",
                minlength: "Min 3 symbol"
            },
            inputPassword:{
                required: "Required field",
                minlength: "Min 3 symbol",
                maxlength: "Max symbol - 16"
            }

        }

    });

})