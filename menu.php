<?php
if(isset($_SESSION['_id'])){
    $data = [

        'Profile' => '/profile.php',
        'Logout' => '/logout.php'
    ];

}else{
    $data = [
        'Login' => '/login.php',
        'Registration' => '/registration.php'
    ];
}