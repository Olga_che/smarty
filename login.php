<?php
session_start();
include_once 'libs/Smarty.class.php';
include_once 'function.php';
$smarty = new Smarty();
include_once "menu.php";
$title = 'Login';
$smarty->assign('menu', $data);
$smarty->assign('title', $title);
if(isset($_SESSION['_id'])){
    header("location:profile.php");
}else{
    include('/configs/db.php');
    if(isset($_POST['inputEmail']) && isset($_POST['inputPassword'])){
        $funt = new AllFunct();
        $funt->login($connect, $_POST['inputEmail'], sha1($_POST['inputPassword']));
    }
}

$smarty->display('templates/layouts/header.tpl');
$smarty->display("templates/site/login.tpl");
$smarty->display("templates/layouts/footer.tpl");
