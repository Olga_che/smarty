<?php
session_start();
include_once 'libs/Smarty.class.php';
include_once "menu.php";
$smarty = new Smarty();
include_once 'function.php';
$smarty->assign('menu', $data);

$title = 'Registration';

$smarty->assign('title', $title);
if (isset($_POST['inputName'])) {
    $inputName = $_POST['inputName'];
    if ($inputName == '') {
        unset($inputName);
    }
}
if (isset($_POST['inputPassword'])) {
    $inputPassword = $_POST['inputPassword'];
    if ($inputPassword =='') {
        unset($inputPassword);
    }
}
if (isset($_POST['inputEmail'])) {
    $inputEmail = $_POST['inputEmail'];
    if ($inputEmail =='') {
        unset($inputEmail);
    }
}
if (isset($_POST['inputPasswordRepeat'])) {
    $inputPasswordRepeat = $_POST['inputPasswordRepeat'];
    if ($inputPasswordRepeat =='') {
        unset($inputPasswordRepeat);
    }
}
if (!isset($inputName) || !isset($inputPassword) || !isset($inputPasswordRepeat) || !isset($inputEmail))
{
    $error = 'Empty fields';
}else{
    $inputName = trim($inputName);
    $inputPassword = trim(sha1($inputPassword));
    $inputPasswordRepeat = trim(sha1($inputPasswordRepeat));
    $inputEmail = trim($inputEmail);
    if($inputPassword != $inputPasswordRepeat ) {
        $error = 'Passwords are not identical';
    }else{
        include('/configs/db.php');
        $a = new AllFunct();
        $result = $a->registration($connect, $inputName, $inputEmail, $inputPassword);
    }
}

$smarty->display('templates/layouts/header.tpl');
$smarty->display("templates/site/registration.tpl");
$smarty->display("templates/layouts/footer.tpl");
